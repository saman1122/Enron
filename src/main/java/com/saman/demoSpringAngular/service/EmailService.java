package com.saman.demoSpringAngular.service;

import com.saman.demoSpringAngular.domain.SearchResult;
import com.saman.demoSpringAngular.entity.Email;
import com.saman.demoSpringAngular.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmailService {

    @Autowired
    EmailRepository emailRepository;

    public Page<SearchResult> getEmailFindByTerm(String term, Pageable pageable) {
        List<SearchResult> result = emailRepository.findAll()
                .stream().filter(email -> email.toString().toLowerCase().contains(term.toLowerCase()))
                .map(email -> {
                    int occurNbr = StringUtils.countOccurrencesOf(email.toString().toLowerCase(), term.toLowerCase());
                    return new SearchResult(email, occurNbr, occurNbr);
                })
                .sorted(Comparator.comparingInt(SearchResult::getOccurencesNumber).reversed())
                .collect(Collectors.toList());

        int fromI = Math.min((int) pageable.getOffset(), result.size());
        int toI = Math.min((int) pageable.getOffset() + pageable.getPageSize(), result.size());

        return new PageImpl<>(result.subList(fromI, toI), pageable, result.size());
    }

    public Page<Email> listAllByPage(Pageable pageable){
        Page<Email> page = emailRepository.findAll(pageable);
        return new PageImpl<>(page.getContent(),pageable,page.getTotalElements());
    }

    public Email getOneById(String id) {
        return emailRepository.findById(id).orElse(null);
    }
}
