package com.saman.demoSpringAngular.entity;


import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Email {
    @Id
    public String messageId;
    public String mailbox;
    public String user;
    @Column(name = "from_address")
    public String from;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<String> to;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<String> cc;
    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<String> bcc;
    public String subject;
    @Column(columnDefinition="TEXT")
    public String content;
    public Date date;

    public Email() {
        this.date = new Date();
    }

    public Email(String messageId, String mailbox, String user, String from, List<String> to, List<String> cc, List<String> bcc, String subject, String content, Date date) {
        this.messageId = messageId;
        this.mailbox = mailbox;
        this.user = user;
        this.from = from;
        this.to = to;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.content = content;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Email{" +
                "messageId='" + messageId + '\'' +
                ", mailbox='" + mailbox + '\'' +
                ", user='" + user + '\'' +
                ", from='" + from + '\'' +
                ", to=" + to +
                ", cc=" + cc +
                ", bcc=" + bcc +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", date='" + date.toString() + '\'' +
                '}';
    }
}
